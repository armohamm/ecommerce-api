/**
 * 
 */
package br.com.massuda.alexander.ecommerce.api.model.produto;

import java.time.LocalDateTime;
import java.util.List;

import br.com.massuda.alexander.ecommerce.api.model.Usuario;
import br.com.massuda.alexander.persistencia.jdbc.anotacoes.ChaveEstrangeira;
import br.com.massuda.alexander.persistencia.jdbc.model.EntidadeModelo;

/**
 * @author Alex
 *
 */
public class Produto extends EntidadeModelo {
	
	/**
	 * 
	 */
	private static final long serialVersionUID = -8896947527533864159L;
	
	private String titulo;
	private String descricao;
	private LocalDateTime criacao = LocalDateTime.now();
	@ChaveEstrangeira(estaEmOutraTabela=true)
	private Usuario responsavel;
	private List<Preco> precos;
	
	public String getTitulo() {
		return titulo;
	}
	public void setTitulo(String titulo) {
		this.titulo = titulo;
	}
	public String getDescricao() {
		return descricao;
	}
	public void setDescricao(String descricao) {
		this.descricao = descricao;
	}
	public LocalDateTime getCriacao() {
		return criacao;
	}
	public void setCriacao(LocalDateTime criacao) {
		this.criacao = criacao;
	}
	public Usuario getResponsavel() {
		return responsavel;
	}
	public void setResponsavel(Usuario responsavel) {
		this.responsavel = responsavel;
	}
	public List<Preco> getPrecos() {
		return precos;
	}
	public void setPrecos(List<Preco> precos) {
		this.precos = precos;
	}
	

}
