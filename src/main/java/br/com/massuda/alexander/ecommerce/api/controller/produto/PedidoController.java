/**
 * 
 */
package br.com.massuda.alexander.ecommerce.api.controller.produto;

import java.sql.SQLException;
import java.time.LocalDateTime;
import java.util.List;

import javax.validation.Valid;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.stereotype.Controller;
import org.springframework.util.ObjectUtils;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.ResponseStatus;

import br.com.massuda.alexander.ecommerce.api.dao.DAO;
import br.com.massuda.alexander.ecommerce.api.dao.Finder;
import br.com.massuda.alexander.ecommerce.api.mapper.MapperUsuario;
import br.com.massuda.alexander.ecommerce.api.model.produto.Pedido;
import br.com.massuda.alexander.ecommerce.api.servico.ServicoUsuario;
import br.com.massuda.alexander.usuario.orm.modelo.usuario.Usuario;
import br.com.waiso.framework.exceptions.ErroUsuario;

/**
 * @author Alex
 *
 */
@Controller
@RequestMapping("/pedido")
public class PedidoController {
	
	@Autowired
	ServicoUsuario servicoUsuario;
	
	@PostMapping
	@ResponseStatus(code = HttpStatus.OK)
	public void incluir(@Valid @RequestBody Pedido o) throws ErroUsuario, SQLException {
		DAO<Pedido> dao = new DAO<Pedido>(Pedido.class) {};
		
		Usuario cliente = servicoUsuario.pesquisarPorId(o.getCliente().getId());//servicoUsuario.pesquisarPorLogin(tarefa.getCriador().getLogin());
		o.setCliente(MapperUsuario.to(cliente));
		dao.incluir(o);
	}
	
	@PutMapping
	@ResponseStatus(code = HttpStatus.OK)
	public void editar(@Valid @RequestBody Pedido o) throws ErroUsuario, SQLException {
		DAO<Pedido> dao = new DAO<Pedido>(Pedido.class) {};
		
		Usuario cliente = servicoUsuario.pesquisarPorId(o.getCliente().getId());//servicoUsuario.pesquisarPorLogin(tarefa.getCriador().getLogin());
		o.setCliente(MapperUsuario.to(cliente));
		dao.editar(o);
	}
	
	@ResponseBody
	@GetMapping
	public List<Pedido> listar() {
		Finder<Pedido> finder = new Finder<Pedido>(Pedido.class) {};
		
		List<Pedido> lista = finder.listar();
		for (int indice = 0; indice < lista.size(); indice++) {
			Usuario responsavel = null;
			if (ObjectUtils.isEmpty(lista.get(indice).getCliente())) {
				responsavel = servicoUsuario.pesquisarPorId(lista.get(indice).getCliente().getId());
			}
			lista.get(indice).setCliente(MapperUsuario.from(responsavel));
		}
		
		return lista;
	}
	
	@ResponseBody
	@GetMapping("/{id}")
	public Pedido pesquisarTarefa(@PathVariable long id) {
		Finder<Pedido> finder = new Finder<Pedido>(Pedido.class) {};
		Pedido o = finder.pesquisar(id);
		br.com.massuda.alexander.ecommerce.api.model.Usuario cliente = MapperUsuario.from(servicoUsuario.pesquisarPorId(o.getCliente().getId()));
		o.setCliente(cliente);
		return o;
	}
	
	@ResponseBody
	@GetMapping("/usuario/{usuario}")
	public List<Pedido> listarTarefa(@PathVariable long usuario, LocalDateTime inicio, LocalDateTime fim) {
		Finder<Pedido> finder = new Finder<Pedido>(Pedido.class) {};
		return finder.listar();
	}
	
}
