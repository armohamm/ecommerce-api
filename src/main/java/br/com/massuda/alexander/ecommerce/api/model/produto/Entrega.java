/**
 * 
 */
package br.com.massuda.alexander.ecommerce.api.model.produto;

import java.util.List;

import br.com.massuda.alexander.persistencia.jdbc.model.EntidadeModelo;
import br.com.massuda.alexander.usuario.orm.modelo.Endereco;

/**
 * @author Alex
 *
 */
public class Entrega extends EntidadeModelo {

	/**
	 * 
	 */
	private static final long serialVersionUID = -7477801360798647363L;
	
	private List<Endereco> enderecos;
	private Entregadora entregadora;
	
	public List<Endereco> getEnderecos() {
		return enderecos;
	}
	public void setEnderecos(List<Endereco> enderecos) {
		this.enderecos = enderecos;
	}
	public Entregadora getEntregadora() {
		return entregadora;
	}
	public void setEntregadora(Entregadora entregadora) {
		this.entregadora = entregadora;
	}

}
