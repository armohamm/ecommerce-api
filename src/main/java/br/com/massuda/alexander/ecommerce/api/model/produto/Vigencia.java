/**
 * 
 */
package br.com.massuda.alexander.ecommerce.api.model.produto;

import java.time.LocalDateTime;

import br.com.massuda.alexander.persistencia.jdbc.model.EntidadeModelo;

/**
 * @author Alex
 *
 */
public class Vigencia extends EntidadeModelo {

	/**
	 * 
	 */
	private static final long serialVersionUID = 3315985259999318174L;
	private LocalDateTime inicio;
	private LocalDateTime fim;
	
	public LocalDateTime getInicio() {
		return inicio;
	}
	public void setInicio(LocalDateTime inicio) {
		this.inicio = inicio;
	}
	public LocalDateTime getFim() {
		return fim;
	}
	public void setFim(LocalDateTime fim) {
		this.fim = fim;
	}
	
}
