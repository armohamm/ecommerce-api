/**
 * 
 */
package br.com.massuda.alexander.ecommerce.api.controller;

import java.util.List;

import javax.validation.Valid;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Null;
import javax.ws.rs.PathParam;

import org.apache.commons.lang3.StringUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.RestController;

import br.com.massuda.alexander.ecommerce.api.servico.ServicoUsuario;
import br.com.massuda.alexander.usuario.orm.modelo.usuario.Usuario;

/**
 * @author Alex
 *
 */
@CrossOrigin
@RestController
@RequestMapping("/usuario")
public class UsuarioController extends ControllerBase {

	@Autowired
	private ServicoUsuario servico;
	
	@ResponseBody
	@RequestMapping("/nome/{nome}")
	public List<Usuario> pesquisarPorNome (@PathParam(value = "nome") @Null String nome) {
		nome = !StringUtils.isEmpty(nome) ? nome : StringUtils.EMPTY;
		return servico.pesquisarPorNome(nome);
	}
	

	@ResponseBody
	@RequestMapping("/id/{id}")
	public Usuario pesquisarPorId (@Valid @PathParam(value = "id") @NotNull int id) {
		return servico.pesquisarPorId(id);
	}
	
}
